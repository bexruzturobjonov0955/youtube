# 1
FROM node:14 AS build

WORKDIR /app

COPY package*.json ./

# REACT_APP_RAPID set as variable in gitlab. You need to set also, you can get API easily for free from https://rapidapi.com/ytdlfree/api/youtube-v31/
ARG $REACT_APP_RAPID_API_KEY


RUN npm install

COPY . .

RUN npm run build

# 2.
FROM nginx:alpine

# Nginx conf required
COPY nginx.conf /etc/nginx/conf.d/default.conf

COPY --from=build /app/build /usr/share/nginx/html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]